#!/bin/bash

# Replace environment variables
# This is mandatory as at the time of doing the migration, discourse needs to know redis IP,
# which is set thanks to discourse.conf file.
echo "--> Overwritting env variables ..."
envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/config/discourse.conf
echo "--> DONE"

# Ensure that assigned uid has entry in /etc/passwd.
# ffi:  https://gitlab.cern.ch/webservices/discourse-cern/-/issues/7
if [ `id -u` -ge 10000 ]; then
  cat /etc/passwd | sed -e "s/^discourse:/builder:/" > /tmp/passwd
  echo "discourse:x:`id -u`:`id -g`:,,,:/home/discourse:/bin/bash" >> /tmp/passwd
  cat /tmp/passwd > /etc/passwd
  rm /tmp/passwd
fi

# Migrate db and precompile assets
exec env RAILS_ENV="production" bundle exec rake db:migrate themes:update assets:precompile