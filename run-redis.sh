#!/bin/bash

### Configure Redis
### n.b.: https://github.com/discourse/discourse_docker/blob/master/templates/redis.template.yml
### Note that /shared/redis_data is configured as emptyDir.

exec /usr/bin/redis-server /etc/redis/redis.conf