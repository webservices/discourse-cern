#!/bin/bash

# Replace environment variables
echo "--> Overwritting env variables ..."
envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/config/discourse.conf
echo "--> DONE"

export RAILS_ENV="production"

# Ensure that assigned uid has entry in /etc/passwd.
# ffi:  https://gitlab.cern.ch/webservices/discourse-cern/-/issues/7
if [ `id -u` -ge 10000 ]; then
  cat /etc/passwd | sed -e "s/^discourse:/builder:/" > /tmp/passwd
  echo "discourse:x:`id -u`:`id -g`:,,,:/home/discourse:/bin/bash" >> /tmp/passwd
  cat /tmp/passwd > /etc/passwd
  rm /tmp/passwd
fi

# Precompile assets. If succees, proceed with Nginx.
echo "--> Running nginx ..."
exec nginx -g "daemon off;"
