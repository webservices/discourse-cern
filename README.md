# Openshift Template for Discourse

This template will create the basic structure of an OpenShift service running Discourse. It will allow you to rapidly deploy an instance of the forum just configuring a few things.

The real project comes from [https://www.discourse.org/](https://www.discourse.org/).

## How to create a new instance

In order to create a new instance of discourse, please open a ticket at [Discourse Service](https://cern.service-now.com/service-portal/report-ticket.do?name=request&se=discourse).

We will guide you over the next steps to have your instance ready!

## Template Administators documentation

Can be found under [https://discourse-docs.web.cern.ch](https://discourse-docs.web.cern.ch).
