# Check https://github.com/discourse/discourse_docker/blob/master/launcher
FROM discourse/base:2.0.20220128-1817

LABEL maintainer="Discourse Administrators <discourse-admins@cern.ch>" \
      io.openshift.expose-services="8080,3000,6379:http"

ENV PG_MAJOR 11
ENV RUBY_ALLOCATOR /usr/lib/libjemalloc.so.1
ENV RAILS_ENV production

### Install prerequisites #############################
# Install PG_11 for CERN
# n.b.: https://github.com/discourse/discourse_docker/commit/82e4d76f8bb93a3517a71ebb0c710307101476be
RUN DEBIAN_FRONTEND=noninteractive apt-get purge -y postgresql-13 postgresql-client-13 postgresql-contrib-13 && \
    apt -y update && \
    apt -y install postgresql-${PG_MAJOR} postgresql-client-${PG_MAJOR} \
    postgresql-contrib-${PG_MAJOR} \
    # Use gettext for envsubst
    gettext
RUN apt clean

### Configure Nginx
### n.b.: https://www.redpill-linpro.com/sysadvent/2017/12/10/jekyll-openshift.html
### /var/run is configured for different pids. Check unicorn.conf.rb and sidekiq.yml configurations.
RUN \
  ln -sf /dev/stdout /var/log/nginx/access.log && \
  ln -sf /dev/stderr /var/log/nginx/error.log && \
  mkdir -p /etc/nginx/ /var/run /var/cache/nginx /var/lib/nginx /var/log/nginx && \
  chgrp -R 0 /etc/nginx/ /var/run /var/cache/nginx /var/lib/nginx /var/log/nginx && \
  chmod -R g=u /etc/nginx/ /var/run /var/cache/nginx /var/lib/nginx /var/log/nginx

### Configure Redis
RUN \
    chgrp -R 0 /var/lib/redis && \
    chmod -R g=u /var/lib/redis && \
    # Follow up on https://github.com/discourse/discourse_docker/blob/master/templates/redis.template.yml
    sed -i 's|daemonize yes|daemonize no|' /etc/redis/redis.conf && \
    sed -i 's/^pidfile.*$//' /etc/redis/redis.conf && \
    sed -i 's/^logfile.*$/logfile \"\"/' /etc/redis/redis.conf && \
    sed -i 's/^bind .*$//' /etc/redis/redis.conf && \
    sed -i 's/^dir .*$/dir \/shared\/redis_data\//' /etc/redis/redis.conf && \
    sed -i 's/^protected-mode yes/protected-mode no/' /etc/redis/redis.conf
    
### Discourse specific bits

# Install modified version of Discourse
# we can override it with --build_arg
ARG DISCOURSE_VERS='tests-passed'
ENV DISCOURSE_RELEASE=$DISCOURSE_VERS RAILS_ROOT=/discourse HOME=/discourse

# Remove upstream discourse installation, so we will pre-cook ours.
RUN rm -rf /var/www/discourse && \
    mkdir -p discourse && \
    git clone --single-branch --branch "$DISCOURSE_RELEASE"  https://github.com/discourse/discourse.git /discourse    

### Discourse config
ADD config/unicorn.conf.rb /discourse/config/unicorn.conf.rb
# Go to /tmp to be ready for envsubst
ADD config/discourse.conf /tmp/discourse-configmap/discourse.conf
ADD config/sidekiq.yml /tmp/discourse-configmap/sidekiq.yml

# Nginx config
ADD config/nginx.conf /etc/nginx/nginx.conf
ADD config/discourse-nginx.conf /etc/nginx/conf.d/nginx.conf

# ImageMagick config
# c.f.: https://github.com/discourse/discourse_docker/commit/7b3d1c513f833a0758ba1f647436514dd365bfd0 
ADD config/policy.xml /usr/local/etc/ImageMagick-7/

WORKDIR $HOME

### Plugins
#
#   - OAuth
RUN \
    git clone --depth=1 https://github.com/discourse/discourse-oauth2-basic.git /discourse/plugins/discourse-oauth2-basic && \
#   - Chat Integration
    git clone --depth=1 https://github.com/discourse/discourse-chat-integration.git /discourse/plugins/discourse-chat-integration && \
#   - Discourse Solved
    git clone --depth=1 https://github.com/discourse/discourse-solved.git /discourse/plugins/discourse-solved && \
#   - Trade buttons (used on Marketplace)
    git clone --depth=1 https://github.com/jannolii/discourse-topic-trade-buttons.git /discourse/plugins/discourse-topic-trade-buttons && \
#   - Saved Searches
    git clone --depth=1 https://github.com/discourse/discourse-saved-searches.git /discourse/plugins/discourse-saved-searches && \
#   - Migrate Password
    git clone --depth=1 https://github.com/discoursehosting/discourse-migratepassword.git /discourse/plugins/discourse-migratepassword && \
#   - Discourse Math
    git clone --depth=1 https://github.com/discourse/discourse-math.git /discourse/plugins/discourse-math && \
#   - Discourse Askimet
    git clone --depth=1 https://github.com/discourse/discourse-akismet.git /discourse/plugins/discourse-akismet && \
#   - Canned replies
    git clone --depth=1 https://github.com/discourse/discourse-canned-replies.git /discourse/plugins/discourse-canned-replies && \
#   - Discourse JIRA
    git clone --depth=1 https://github.com/karies/discourse-jira.git /discourse/plugins/discourse-jira && \
#   - Prometheus
    git clone --depth=1 https://github.com/discourse/discourse-prometheus.git /discourse/plugins/discourse-prometheus && \
#   - Calendar
    git clone --depth=1 https://github.com/discourse/discourse-calendar.git /discourse/plugins/discourse-calendar && \
#   - Assign
    git clone --depth=1 https://github.com/discourse/discourse-assign.git /discourse/plugins/discourse-assign && \
#   - Discourse push notifications
    git clone --depth=1 https://github.com/discourse/discourse-push-notifications.git /discourse/plugins/discourse-push-notifications && \
#   - Discourse voting
    git clone --depth=1 https://github.com/discourse/discourse-voting.git /discourse/plugins/discourse-voting && \
#   - Discourse data explorer
    git clone --depth=1 https://github.com/discourse/discourse-data-explorer.git /discourse/plugins/discourse-data-explorer && \
#   - Discourse docs
    git clone --depth=1 https://github.com/discourse/discourse-docs.git /discourse/plugins/discourse-docs && \
#   - Discourse graphviz
    git clone --depth=1 https://github.com/discourse/discourse-graphviz.git /discourse/plugins/discourse-graphviz

### Gem installation
RUN bundle config --local deployment true && \
    bundle config --local path ./vendor/bundle && \
    bundle config --local without test development && \
    bundle install --jobs 4 && \
    yarn install --production && \
    yarn cache clean && \
    exec bundle exec rake maxminddb:get && \
    find /discourse/vendor/bundle -name tmp -type d -exec rm -rf {} +

COPY ["cgroup-limits","init-dbmigration.sh","run-discourse.sh","run-nginx.sh","run-redis.sh","./"]
RUN chmod +x ./run-discourse.sh ./run-nginx.sh ./run-redis.sh ./init-dbmigration.sh && \
    chgrp -R 0 /discourse && chmod -R g=u /discourse && \
    chmod +x /discourse/config/unicorn_launcher && \
    # see https://gitlab.cern.ch/webservices/discourse-cern/-/issues/7
    # Adjust permissions on /etc/passwd so writable by group root
    chmod g+w /etc/passwd

### entrypoint will be overriden for init-containers
ENTRYPOINT ["./run-discourse.sh"]